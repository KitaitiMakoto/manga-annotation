file "build/001bj/collection/top.json" => "images/001bj/001bj-210-0.png"

file "imgsrc/001bj.pdf" => "imgsrc/001bj.zip" do |task|
  Archive::Zip.extract task.source, File.dirname(task.name), directories: false, exclude: ->(entry) {entry.zip_path != File.basename(task.name)}
end

file "imgsrc/001bj.zip" => "imgsrc" do |task|
  open "http://mangaonweb.com/dcms_media/other/001bj.zip" do |file|
    File.write task.name, file.read
  end
end

file "images/001bj/001bj-210-0.png" => ["imgsrc/001bj.pdf", "images/001bj"] do |task|
  doc = Poppler::Document.new(task.source)
  num_pages = doc.pages.length
  page_num_of_digits = Math.log10(num_pages).to_i + 1
  doc.each_with_index do |page, page_index|
    num_images = page.image_mapping.length
    image_num_of_digits = Math.log10(num_images).to_i + 1
    page.image_mapping.each_with_index do |mapping, image_index|
      basename = "%s-%0#{page_num_of_digits}d-%0#{image_num_of_digits}d.png" % [File.basename(task.source).ext(""), page_index, image_index]
      path = File.join(task.sources[1], basename)
      $stderr.puts "extracting #{path}"
      mapping.image.write_to_png(path)
    end
  end
end
