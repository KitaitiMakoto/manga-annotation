use actix_web::{web, App, HttpResponse, HttpServer, Responder, Result};
use serde::{Serialize, Deserialize};
use serde_json;

#[derive(Deserialize)]
struct NewAnnotation {
    #[serde(rename = "@context")]
    context: String,
    r#type: String,
    body: String,
    target: String,
}

#[derive(Serialize)]
struct Annotation<'a> {
    context: &'a str,
    id: &'a str,
    r#type: &'a str,
    body: &'a str,
    target: &'a str,
}

fn get_annotations() -> impl Responder {
    HttpResponse::Ok().body("{}")
}

fn create_annotation(new_anno: web::Json<NewAnnotation>) -> Result<String> {
    let anno = Annotation {
        context: &new_anno.context,
        id: "",
        r#type: &new_anno.r#type,
        body: &new_anno.body,
        target: &new_anno.target,
    };
    Ok(serde_json::to_string(&anno)?)
}

fn main() {
    HttpServer::new(|| {
        App::new()
            .route("/annotations/", web::get().to(get_annotations))
            .route("/annotations/", web::post().to(create_annotation))
    })
    .bind("127.0.0.1:8088")
    .unwrap()
    .run()
    .unwrap();
}
