# coding: utf-8
require "yaml"
require "open-uri"
require "archive/zip"
require "poppler"
require "csv"
require "wax_iiif"
require "erb"

def base_url
  ENV["BASE_URL"] || WaxIiif::Config::DEFAULT_URL
end

def define_tasks(name, config)
  desc "Build #{name}"
  task name => "build/#{name}/basic.json"

  file "build/#{name}/basic.json" => "build/#{name}/collection/top.json" do |task|
    canvases = Dir.glob("build/#{name}/canvas/*.json").sort.collect {|filename| JSON.load(open(filename))}

    manifest = {
      "@context" => "http://iiif.io/api/presentation/2/context.json",
      "@id" => "#{base_url}/#{name}/#{File.basename(task.name)}",
      "@type" => "sc:Manifest",
      "attribution" => config["attribution"],
      "license" => config["license"],
      "description" => config["description"],
      "label" => config["label"],
      "metadata" => config["metadata"],
      "viewingDirection" => "right-to-left",
      "viewingHint" => "paged",
      "thumbnail" => canvases.first["thumbnail"],
      "sequences" => [
        {
          "@type": "sc:Sequence",
         "canvases": canvases
        }
      ],
      "service" => {
        "@context" => "http://iiif.io/api/search/0/context.json",
        "@id" => config["search"],
        "profile" => "http://iiif.io/api/search/0/search"
      }
    }
    File.write task.name, manifest.to_json
  end

  file "build/#{name}/collection/top.json" do |task|
    builder = WaxIiif::Builder.new(
      base_url: "#{base_url}/#{name}",
      output_dir: "build/#{name}",
      verbose: true
    )
    records = Dir.glob("images/#{name}/**").collect {|path|
      WaxIiif::ImageRecord.new(
        id: File.basename(path),
        path: path,
        is_primary: true,
        attribution: config["attribution"],
        license: config["license"],
        description: config["description"],
        label: config["label"],
        metadata: config["metadata"]
      )
    }
    builder.load records
    builder.process_data
  end

  directory "build/#{name}" => "build"
  directory "images/#{name}" => "images"

  import "#{name}.rake" if File.exist? "#{name}.rake"
end

names = []
Dir.glob("config/*.yaml").each do |path|
  config = YAML.load_file(path)
  name = File.basename(path).ext("")
  names << name
  define_tasks name, config
end

task :default => %w[build/index.html build/mirador2.html build/mirador3.html] + names

file "build/index.html" => ["templates/index.html.erb", "build"] do |task|
  template = ERB.new(File.read(task.source))
  File.write task.name, template.result(binding)
end

file "build/mirador3.html" => ["templates/mirador3.html.erb", "build"] do |task|
  keys = Dir.glob("config/*.yaml").collect {|path| File.basename(path).ext("")}
  template = ERB.new(File.read(task.source))
  File.write task.name, template.result(binding)
end

file "build/mirador2.html" => ["templates/mirador2.html.erb", "build"] do |task|
  keys = Dir.glob("config/*.yaml").collect {|path| File.basename(path).ext("")}
  template = ERB.new(File.read(task.source))
  File.write task.name, template.result(binding)
end

directory "build"

directory "images"

directory "imgsrc"
